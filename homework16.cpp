﻿#include <iostream>
#include <time.h>

const int N = 5;
int array[N][N];


int main()
{
    struct tm buf;
    time_t t = time(NULL);
    localtime_s(&buf, &t);
    
    for (int i = 0; i < N; i++)
    {
        int sum = 0;
        for (int j = 0; j < N; j++)
        {
            array[i][j] = i + j;
            std::cout << array[i][j] << " ";
            sum += array[buf.tm_mday % N][j];
        }
        if (i == buf.tm_mday % N)
            std::cout << std::endl << sum << '\n';
        else
            std::cout << std::endl;

    }
    
    return 0;
}
